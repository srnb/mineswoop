import sbtcrossproject.CrossPlugin.autoImport.{
  crossProject,
  CrossType
}

val osName: SettingKey[String] = SettingKey[String]("osName")

osName in Global := (System.getProperty("os.name") match {
  case n if n.startsWith("Linux") => "linux"
  case n if n.startsWith("Mac") => "mac"
  case n if n.startsWith("Windows") => "win"
  case _ => throw new Exception("Unknown platform!")
})

val sharedSettings = Seq(
  organization := "tf.bug",
  name         := "mineswoop",
  version      := "0.1.0",
  scalaVersion := "2.12.8",
  scalacOptions ++= Seq(
    "-unchecked",
    "-deprecation",
    "-feature",
    "-Ypartial-unification",
    "-language:higherKinds",
  ),
  libraryDependencies ++= Seq(
    "org.typelevel" %%% "cats-core"   % "1.6.0",
    "org.typelevel" %%% "cats-effect" % "1.2.0",
    "co.fs2" %%% "fs2-core"           % "1.0.3",
  ),
  mainClass := Some("tf.bug.mineswoop.Main"),
)

lazy val mineswoop =
  crossProject(JSPlatform, JVMPlatform)
    .crossType(CrossType.Pure)
    .settings(sharedSettings)
    .jsSettings(scalaJSUseMainModuleInitializer := true)
    .jvmSettings(
      mainClass in assembly := Some("tf.bug.mineswoop.Main")
    )

lazy val mineswoopJS = mineswoop.js
lazy val mineswoopJVM = mineswoop.jvm

val javaFXTargets =
  Seq("base", "controls", "media", "graphics", "fxml")

val jfxSettings = Seq(
  organization := "tf.bug",
  name         := "mineswoop-jfx",
  version      := "0.1.0",
  scalaVersion := "2.12.8",
  scalacOptions ++= Seq(
    "-unchecked",
    "-deprecation",
    "-feature",
    "-Ypartial-unification",
    "-language:higherKinds",
  ),
  libraryDependencies ++= Seq(
    "org.scalafx" %%% "scalafx" % "11-R16",
  ) ++ javaFXTargets.map { n =>
    "org.openjfx" % s"javafx-$n" % "11.0.2" classifier (osName in Global).value
  },
  fork in run := true,
  javaHome    := Some(file("/usr/lib/jvm/java-11-openjdk/")),
  mainClass   := Some("tf.bug.mineswoop.jfx.Main"),
)

lazy val mineswoopJfx =
  crossProject(JVMPlatform)
    .crossType(CrossType.Full)
    .settings(jfxSettings)
    .jvmSettings( /* ... */ )
    .dependsOn(mineswoop)

lazy val mineswoopJfxJVM = mineswoopJfx.jvm
