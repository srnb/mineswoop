package tf.bug.mineswoop

import cats.data.StateT
import cats.{Applicative, Monad}

import scala.annotation.strictfp
import scala.collection.generic.CanBuildFrom

case class Seed(seed: Long) extends AnyVal

object Seed {

  def long[F[_]: Applicative]: StateT[F, Seed, Long] = StateT { s =>
    Applicative[F]
      .pure(
        Seed(s.seed * 6364136223846793005L + 1442695040888963407L),
        s.seed
      )
  }

  def int[F[_]: Applicative](n: Int): StateT[F, Seed, Int] =
    long.map(l => (((l % n) + n) % n).toInt)

  def boolean[F[_]: Applicative]: StateT[F, Seed, Boolean] =
    long.map(_ >= 0)

  @strictfp def double[F[_]: Applicative]: StateT[F, Seed, Double] =
    long.map(
      l =>
        l.toBinaryString.reverse
          .padTo(64, '0')
          .reverse
          .zipWithIndex
          .map {
            case (c, ind) =>
              c.toString.toInt * Math.pow(2.0, -(ind + 1))
          }
          .sum
    )

  // Adapted from scala.util.Random
  def shuffle[F[_]: Monad, T, CC[X] <: TraversableOnce[X]](
    xs: CC[T]
  )(
    implicit bf: CanBuildFrom[CC[T], T, CC[T]]
  ): StateT[F, Seed, CC[T]] = {
    import cats.data._
    import cats.implicits._
    def swap(e: List[T])(a: Int, b: Int): List[T] = {
      val ea = e(a)
      val eb = e(b)
      val eua = e.updated(a, eb)
      val eub = eua.updated(b, ea)
      eub
    }
    val s: List[StateT[F, Seed, (Int, Int)]] =
      (xs.size to 2 by -1).toList
        .map(n => Seed.int[F](n).map(k => (n - 1, k)))
    val sequenced: StateT[F, Seed, List[(Int, Int)]] = s.sequence
    val folded = sequenced.map(_.foldLeft(xs.toList) {
      case (l, (a, b)) => swap(l)(a, b)
    })
    val built = folded.map(bf().++=(_).result())
    built
  }

}
