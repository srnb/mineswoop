package tf.bug.mineswoop.grid

import tf.bug.mineswoop.model.{Cell, CellCat, CellState}

case class GridCell[F[_]](
  state: CellState,
  cat: CellCat,
  position: Vec2[F]
) extends Cell[F, GridCell[F], Vec2[F]]
