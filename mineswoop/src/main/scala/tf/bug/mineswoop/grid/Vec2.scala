package tf.bug.mineswoop.grid

import cats.Monad
import cats.data.StateT
import fs2._
import tf.bug.mineswoop.Seed
import tf.bug.mineswoop.model.Position

import scala.collection.immutable

case class Vec2[F[_]: Monad](x: Int, y: Int)
    extends Position[F, Vec2[F]] {

  lazy val pureNeighbors: immutable.IndexedSeq[Vec2[F]] = for {
    px <- (x - 1) to (x + 1)
    py <- (y - 1) to (y + 1)
  } yield Vec2[F](px, py)

  override def shuffledNeighbors
    : StateT[F, Seed, Stream[F, Vec2[F]]] = {
    val nonShuffled = pureNeighbors
    Seed
      .shuffle[F, Vec2[F], immutable.IndexedSeq](nonShuffled)
      .map(Stream.emits)
  }

  override def neighbors: Stream[F, Vec2[F]] =
    Stream.emits(pureNeighbors).filter(_ != this)

}
