package tf.bug.mineswoop

import cats.effect.{ExitCode, IO, IOApp}
import cats.implicits._
import tf.bug.mineswoop.grid.Vec2
import tf.bug.mineswoop.rect.RectangleControl

object Main extends IOApp {

  override def run(args: List[String]): IO[ExitCode] = {
    val control = RectangleControl[IO](10, 10, 10)
    for {
      empty <- control.empty
      unearthO <- control.unearth(Vec2[IO](4, 4))(empty)
      show <- control.show(unearthO)
    } yield show
  }.evalTap(s => IO(println(s))).compile.drain.as(ExitCode.Success)

}
