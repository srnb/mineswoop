package tf.bug.mineswoop.rect

import cats.Monad
import cats.data.StateT
import fs2._
import tf.bug.mineswoop.Seed
import tf.bug.mineswoop.grid.{GridCell, Vec2}
import tf.bug.mineswoop.model.{CellCat, CellState, ScanControl}

import scala.util.Random

case class RectangleControl[F[_]: Monad](
  width: Int,
  height: Int,
  numMines: Int,
  seed: Long = System.currentTimeMillis() + Random.nextLong(),
  curve: Double = 1.5d
) extends ScanControl[F, RectangleBoard[F], GridCell[F], Vec2[F]] {

  override def unearth(
    p: Vec2[F]
  )(b: RectangleBoard[F]): Stream[F, RectangleBoard[F]] = {
    b.cell(p)
      .fold[Option[GridCell[F]]](None)((_, e) => Some(e))
      .flatMap {
        case None =>
          for {
            gt <- generateCell(p, b)
            gn <- generateNeighbors(p, gt)
            ub <- unearth(p)(gn)
          } yield ub
        case Some(GridCell(CellState.Covered, cat, _)) =>
          val nb = b.copy(
            cellMap = b.cellMap + (p -> GridCell(
              CellState.Uncovered,
              cat,
              p
            ))
          )
          val gb = generateNeighbors(p, nb)
          gb.flatMap { g =>
            p.neighbors
              .flatMap(g.cell)
              .forall(_.cat != CellCat.Mine)
              .flatMap {
                case true if cat != CellCat.Mine =>
                  onShuffledNeighbors(
                    p,
                    g,
                    (pos, b) => unearth(pos)(b)
                  )
                case _ => Stream(g)
              }
          }
        case _ => Stream(b)
      }
  }

  override def flag(p: Vec2[F])(
    b: RectangleBoard[F]
  ): Stream[F, RectangleBoard[F]] = Stream {
    val cm = b.cellMap
    val c = cm(p)
    val ns = c.state match {
      case CellState.Flagged => CellState.Covered
      case CellState.Covered => CellState.Flagged
      case CellState.Uncovered => CellState.Uncovered
    }
    val nc = c.copy(ns)
    val nm = cm.updated(p, nc)
    b.copy(nm)
  }

  def onShuffledNeighbors(
    pos: Vec2[F],
    board: RectangleBoard[F],
    f: (Vec2[F], RectangleBoard[F]) => Stream[F, RectangleBoard[F]]
  ): Stream[F, RectangleBoard[F]] = {
    val stateT: StateT[F, Seed, Stream[F, Vec2[F]]] =
      shuffledValidNeighbors(
        pos
      )
    val evaled: Stream[F, (Seed, Stream[F, Vec2[F]])] =
      Stream.eval(stateT.run(board.seed))
    evaled.flatMap {
      case (ns, sn) =>
        val nsb = board.copy(seed = ns)
        sn.fold(Stream[F, RectangleBoard[F]](nsb))(
            (b, p) => b.flatMap(sb => f(p, sb))
          )
          .flatten
    }
  }

  def generateNeighbors(
    pos: Vec2[F],
    board: RectangleBoard[F]
  ): Stream[F, RectangleBoard[F]] = {
    onShuffledNeighbors(pos, board, generateCell)
  }

  def generateCell(
    pos: Vec2[F],
    board: RectangleBoard[F]
  ): Stream[F, RectangleBoard[F]] = {
    board
      .cell(pos)
      .fold[Option[GridCell[F]]](None)((_, e) => Some(e))
      .flatMap {
        case Some(_) => Stream(board)
        case None =>
          val totalCells = width * height
          val generatedCells = board.cellMap.size
          val cellsLeft = totalCells - generatedCells
          val totalMines = numMines
          val generatedMines =
            board.cellMap.values.count(_.cat == CellCat.Mine)
          val minesLeft = totalMines - generatedMines
          val probability =
            Math.pow(minesLeft.toDouble / cellsLeft.toDouble, curve)
          val mark = Stream.eval(Seed.double[F].run(board.seed))
          mark.map {
            case (ns, d) =>
              val genMine = d < probability
              val nc = GridCell(
                CellState.Covered,
                if (genMine) CellCat.Mine else CellCat.Field,
                pos
              )
              val gb = board.copy(board.cellMap + (pos -> nc), ns)
              gb
          }
      }
  }

  override def empty: Stream[F, RectangleBoard[F]] =
    Stream(RectangleBoard(Map(), Seed(seed)))

  override def show(board: RectangleBoard[F]): Stream[F, String] = {
    board.cells.flatMap { c =>
      for {
        nm <- mineNeighbors(board, c)
        tc <- board.cell(c)
        im = tc.cat == CellCat.Mine
        ds = tc.state == CellState.Uncovered
      } yield (c, im, nm, ds)
    }.fold(Map[Vec2[F], String]()) {
        case (map, (vec, isMine, nm, ds)) =>
          (isMine, nm, ds) match {
            case (true, _, true) => map + (vec -> "*")
            case (false, 0, true) => map + (vec -> "")
            case (false, n, true) => map + (vec -> n.toString)
            case (_, _, false) => map + (vec -> "??")
          }
      }
      .map { m =>
        (0 until height).toVector.map { y =>
          (0 until width).toVector.map { x =>
            m.getOrElse(Vec2[F](x, y), "##")
          }
        }
      }
      .map { g =>
        val line = "+--" * width ++ "+"
        g.map { row =>
          row
            .map(_.reverse.padTo(2, ' ').reverse)
            .mkString("|", "|", "|")
        }.mkString(line ++ "\n", "\n" ++ line ++ "\n", "\n" ++ line)
      }
  }

  override def validPosition(pos: Vec2[F]): Boolean = {
    val Vec2(x, y) = pos
    x >= 0 && x < width && y >= 0 && y < height
  }

}
