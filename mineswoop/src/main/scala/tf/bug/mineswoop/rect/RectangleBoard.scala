package tf.bug.mineswoop.rect

import fs2._
import tf.bug.mineswoop.Seed
import tf.bug.mineswoop.grid.{GridCell, Vec2}
import tf.bug.mineswoop.model.Board

case class RectangleBoard[F[_]](
  cellMap: Map[Vec2[F], GridCell[F]],
  seed: Seed
) extends Board[F, GridCell[F], Vec2[F]] {

  override def cell(position: Vec2[F]): Stream[F, GridCell[F]] =
    Stream(cellMap.get(position)).filter(_.isDefined).map(_.get)

  override def cells: Stream[F, Vec2[F]] =
    Stream.emits(cellMap.keys.toSeq)

}
