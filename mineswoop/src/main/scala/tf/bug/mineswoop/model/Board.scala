package tf.bug.mineswoop.model

import fs2.Stream

trait Board[F[_], +C <: Cell[F, C, P], P <: Position[F, P]] {

  /** Get a cell
    *
    * @param position The position to be looked up
    * @return An empty stream if that cell has not been
    *         generated or a single element stream if it has
    */
  def cell(position: P): Stream[F, C]

  /**
    * @return A stream of all of the cells on the board
    */
  @deprecated(
    "A board should always be concrete, no need for laziness."
  )
  def cells: Stream[F, P]

}
