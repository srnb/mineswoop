package tf.bug.mineswoop.model

import fs2._

trait ScanControl[F[_], B <: Board[F, C, P], C <: Cell[F, C, P], P <: Position[
  F,
  P
]] extends Control[F, B, C, P] {

  /**
    * @param starting The input board
    * @return A pipe of input unearthings to output boards
    */
  override def unearth(starting: B): Pipe[F, P, B] = { in =>
    in.scan(Stream[F, B](starting)) { (b, p) =>
        b.flatMap(n => unearth(p)(n))
      }
      .flatten
  }

  /**
    * @param p An input position
    * @param b An input board
    * @return An output board with the position
    *         unearthed
    */
  def unearth(p: P)(b: B): Stream[F, B]

  /**
    * @param starting The input board
    * @return A pipe of input flaggings to output boards
    */
  override def flag(starting: B): Pipe[F, P, B] = { in =>
    in.scan(Stream[F, B](starting)) { (b, p) =>
        b.flatMap(n => flag(p)(n))
      }
      .flatten
  }

  /**
    * @param p An input position
    * @param b An input board
    * @return An output board with the position
    *         flagged
    */
  def flag(p: P)(b: B): Stream[F, B]

}
