package tf.bug.mineswoop.model

import cats.Functor
import cats.data.StateT
import fs2._
import tf.bug.mineswoop.Seed

/** A board controller. Manages board states and
  * inputs to a board.
  *
  * @tparam F Effect type
  * @tparam B Board type
  * @tparam C Cell type
  * @tparam P Position type
  */
trait Control[F[_], B <: Board[F, C, P], C <: Cell[F, C, P], P <: Position[
  F,
  P
]] {

  /**
    * @return An empty board
    */
  def empty: Stream[F, B]

  /**
    * @param starting The input board
    * @return A pipe of input unearthings to output boards
    */
  def unearth(starting: B): Pipe[F, P, B]

  /**
    * @param starting The input board
    * @return A pipe of input flaggings to output boards
    */
  def flag(starting: B): Pipe[F, P, B]

  /**
    * @param board The input board
    * @return A String representation of said board
    */
  def show(board: B): Stream[F, String]

  /**
    * @param pos A position to test
    * @return Whether said position is valid
    */
  def validPosition(pos: P): Boolean

  /**
    * @param p A center position
    * @return A stream of positions that are all valid
    *         neighbors
    */
  def validNeighbors(p: P): Stream[F, P] =
    p.neighbors.filter(validPosition)

  /**
    * @param p A center position
    * @param functor A Functor instance for F to do filtering
    * @return A State monad describing the transformation of
    *         a [[tf.bug.mineswoop.Seed]] outputting a stream
    *         of valid neighbors in a pseudo-random order
    */
  def shuffledValidNeighbors(
    p: P
  )(implicit functor: Functor[F]): StateT[F, Seed, Stream[F, P]] =
    p.shuffledNeighbors.map(_.filter(validPosition))

  /**
    * @param board The input board
    * @param c The position to test
    * @return The number of neighbors of the position
    *         that are mines on the board
    */
  def mineNeighbors(board: B, c: P): Stream[F, Int] = {
    validNeighbors(c)
      .flatMap(board.cell)
      .fold(0)((n, c) => if (c.cat == CellCat.Mine) n + 1 else n)
  }

}
