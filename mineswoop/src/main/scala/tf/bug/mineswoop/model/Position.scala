package tf.bug.mineswoop.model

import cats.data.StateT
import fs2.Stream
import tf.bug.mineswoop.Seed

trait Position[F[_], P <: Position[F, P]] {

  /**
    * @return A State monad describing the transformation of
    *         a [[tf.bug.mineswoop.Seed]] outputting a stream
    *         of neighbors in a pseudo-random order
    */
  def shuffledNeighbors: StateT[F, Seed, Stream[F, P]]

  /**
    * @return A stream of neighbors output in a
    *         predetermined order
    */
  def neighbors: Stream[F, P]

}
