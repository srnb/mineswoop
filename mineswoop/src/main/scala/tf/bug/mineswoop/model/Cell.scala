package tf.bug.mineswoop.model

trait Cell[F[_], +C <: Cell[F, C, P], P <: Position[F, P]] {

  /**
    * @return The state of this cell
    */
  def state: CellState

  /**
    * @return The category of this cell
    */
  def cat: CellCat

  /**
    * @return The position of this cell
    */
  def position: P

}

/** Enum that describes all the possible visual states
  * a cell could be in */
sealed trait CellState

object CellState {
  case object Covered extends CellState
  case object Uncovered extends CellState
  case object Flagged extends CellState
}

/** Enum that describes all the possible types
  * a cell could be */
sealed trait CellCat

object CellCat {
  case object Mine extends CellCat
  case object Field extends CellCat
}
