addSbtPlugin(
  "org.portable-scala" % "sbt-scalajs-crossproject" % "0.6.0"
)
addSbtPlugin("org.scala-js" % "sbt-scalajs"  % "0.6.26")
addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "0.14.9")
