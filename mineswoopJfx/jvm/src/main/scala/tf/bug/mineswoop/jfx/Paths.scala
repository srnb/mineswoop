package tf.bug.mineswoop.jfx

import scalafx.scene.Group
import scalafx.scene.shape.SVGPath

case class Paths(mineSize: Double) {

  def p(s: String, c: String): SVGPath = {
    val sv = new SVGPath
    sv.setContent(s)
    sv.setStyle(s"-fx-fill:$c;")
    sv
  }

  def g(g: SVGPath*): Group = {
    val r = new Group(g: _*)
    r.setScaleX(mineSize)
    r.setScaleY(mineSize)
    r
  }

  def uncovered: Group = g(
    p("M0 0v1L1 0z", "#a0a0a0"),
    p("M0 1L1 0v1z", "#f0f0f0"),
    p("M.1.1h.8v.8H.1z", "#c8c8c8"),
  )

  def covered: Group = g(
    p("M0 0v1L1 0z", "#b4b4b4"),
    p("M0 1L1 0v1z", "#646464"),
    p("M.1.1h.8v.8H.1z", "#8c8c8c"),
  )

  def mine: Group = g(
    p("M0 0v1L1 0z", "#a0a0a0"),
    p("M0 1L1 0v1z", "#f0f0f0"),
    p("M.1.1h.8v.8H.1z", "#c8c8c8"),
    p("M.2.5a.3.3 0 1 0 .6 0 .3.3 0 1 0-.6 0", "#202020"),
    p("M.3.4a.1.1 0 1 0 .2 0 .1.1 0 1 0-.2 0", "#e8e8e8"),
  )

  def one: Group = g(
    p("M0 0v1L1 0z", "#a0a0a0"),
    p("M0 1L1 0v1z", "#f0f0f0"),
    p("M.1.1h.8v.8H.1z", "#c8c8c8"),
    p(
      "M.29.79h.167V.217L.275.254V.16l.18-.036h.103V.79h.167v.085H.29z",
      "#0000ff"
    ),
  )

  def two: Group = g(
    p("M0 0v1L1 0z", "#a0a0a0"),
    p("M0 1L1 0v1z", "#f0f0f0"),
    p("M.1.1h.8v.8H.1z", "#c8c8c8"),
    p(
      "M.39.791h.335v.084h-.45V.791L.424.634q.094-.1.118-.128.046-.054.064-.09Q.625.377.625.341q0-.06-.04-.096Q.545.209.482.209q-.045 0-.095.016T.28.275V.174Q.338.15.388.137.438.125.48.125q.11 0 .176.057Q.72.24.72.335q0 .046-.016.087-.017.04-.06.095Q.633.532.57.6L.39.791z",
      "#008000"
    )
  )

  def three: Group = g(
    p("M0 0v1L1 0z", "#a0a0a0"),
    p("M0 1L1 0v1z", "#f0f0f0"),
    p("M.1.1h.8v.8H.1z", "#c8c8c8"),
    p(
      "M.584.471Q.65.486.687.534.725.58.725.65q0 .107-.07.165-.07.059-.197.059-.043 0-.09-.01Q.325.858.276.84V.745Q.313.768.359.78q.046.012.096.012.087 0 .132-.037Q.633.72.633.651q0-.064-.043-.1Q.548.515.473.515h-.08v-.08h.083q.069 0 .105-.03Q.617.378.617.324q0-.056-.038-.086Q.542.207.473.207.435.207.39.216.348.225.296.243V.156Q.348.14.394.133.44.125.481.125q.105 0 .167.05.061.051.061.137 0 .06-.033.102Q.644.455.584.471z",
      "#c00000"
    ),
  )

  def four: Group = g(
    )

  def five: Group = g(
    )

  def six: Group = g(
    )

  def seven: Group = g(
    )

  def eight: Group = g(
    )

  def flag: Group = g(
    p("M0 0v1L1 0z", "#b4b4b4"),
    p("M0 1L1 0v1z", "#646464"),
    p("M.1.1h.8v.8H.1z", "#8c8c8c"),
    p("M.57.27h.12v.58H.57z", "#202020"),
    p("M.7.5L.2.32.7.15z", "#ff0000"),
  )

}
