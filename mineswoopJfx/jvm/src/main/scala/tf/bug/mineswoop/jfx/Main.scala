package tf.bug.mineswoop.jfx

import cats.effect.implicits._
import cats.effect.{ConcurrentEffect, IO}
import fs2._
import fs2.concurrent.Queue
import javafx.scene.input.MouseButton
import scalafx.application.JFXApp.PrimaryStage
import scalafx.application.{JFXApp, Platform}
import scalafx.scene.control.{Button, ContentDisplay}
import scalafx.scene.layout._
import scalafx.scene.paint.Color._
import scalafx.scene.{Group, Scene}
import tf.bug.mineswoop.grid.{GridCell, Vec2}
import tf.bug.mineswoop.model._
import tf.bug.mineswoop.rect.{RectangleBoard, RectangleControl}

import scala.collection.mutable
import scala.concurrent.ExecutionContext

object Main extends JFXApp {

  val mineSize = 32
  val minesW = 20
  val minesH = 20
  val p = Paths(mineSize)

  def paths[F[_]: ConcurrentEffect](
    board: RectangleBoard[F],
    cell: GridCell[F]
  ): Group = {
    cell match {
      case GridCell(CellState.Covered, _, _) => p.covered
      case GridCell(CellState.Uncovered, CellCat.Mine, _) => p.mine
      case GridCell(CellState.Uncovered, CellCat.Field, _) =>
        cell.position.pureNeighbors
          .map(board.cellMap.get)
          .filter(_.isDefined)
          .map(_.get)
          .foldLeft(0)(
            (acc, c) => if (c.cat == CellCat.Mine) acc + 1 else acc
          ) match {
          case 0 => p.uncovered
          case 1 => p.one
          case 2 => p.two
          case 3 => p.three
          case 4 => p.four
          case 5 => p.five
          case 6 => p.six
          case 7 => p.seven
          case 8 => p.eight
        }
      case GridCell(CellState.Flagged, _, _) => p.flag
    }
  }

  def minesweeperGrid[F[_]: ConcurrentEffect](
    control: RectangleControl[F]
  ): (GridPane, Map[Vec2[F], Button], Stream[F, RectangleBoard[F]]) = {
    val eventQueue =
      Queue.unbounded[F, (Boolean, Vec2[F])].toIO.unsafeRunSync()
    val board = control.empty
    val cells = mutable.Map[Vec2[F], Button]()
    (0 until minesW).foreach { x =>
      (0 until minesH).foreach { y =>
        val b = new Button("")
        b.setMinSize(mineSize, mineSize)
        b.setMaxSize(mineSize, mineSize)
        b.setContentDisplay(ContentDisplay.GraphicOnly)
        b.setGraphic(p.covered)
        cells += (Vec2[F](x, y) -> b)
      }
    }
    val mc = cells.toMap
    val pane = new GridPane {
      columnConstraints =
        (0 until minesW).map(_ => new ColumnConstraints(mineSize))
      rowConstraints =
        (0 until minesH).map(_ => new RowConstraints(mineSize))
    }
    mc.foreach {
      case (v @ Vec2(x, y), b) =>
        pane.add(b, x, y)
        b.onMouseClicked = e => {
          e.getButton match {
            case MouseButton.PRIMARY =>
              eventQueue.enqueue1((true, v)).toIO.unsafeRunSync()
            case MouseButton.SECONDARY =>
              eventQueue.enqueue1((false, v)).toIO.unsafeRunSync()
          }

        }
    }
    (
      pane,
      mc,
      eventQueue.dequeue
        .scan(board) {
          case (b, (event, vec)) =>
            if (event) b.flatMap(r => control.unearth(vec)(r))
            else b.flatMap(r => control.flag(vec)(r))
        }
        .flatten
    )
  }

  def updateButtons[F[_]: ConcurrentEffect](
    grid: Map[Vec2[F], Button],
    board: RectangleBoard[F]
  ): Unit = {
    val gbs = board.cellMap.map {
      case (pos, c) => (grid(pos), paths(board, c))
    }
    Platform.runLater {
      gbs.foreach {
        case (button, pathsG) => button.setGraphic(pathsG)
      }
    }
  }

  stage = new PrimaryStage {
    title = "minesweeper"
    scene = new Scene {
      fill = White
      implicit val ioCS = IO.contextShift(ExecutionContext.global)

      val (pane, mc, inputs) = minesweeperGrid[IO](
        RectangleControl(minesW, minesH, (minesW * minesH) / 5)
      )
      content = pane
      Platform.runLater { () =>
        inputs
          .map(updateButtons(mc, _))
          .compile
          .drain
          .unsafeRunAsync(_ => IO.unit)
      }
    }
  }

}
